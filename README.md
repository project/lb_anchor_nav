# Layout Builder Anchor Navigation

This modules provides an anchor navigation layout builder
block that displays an anchor menu for all the blocks on
the page that have the option enabled.

## Steps to add this:

1. Add Anchor navigation block to your page
   where you want the anchor menu to be displayed.
2. Check "Display Anchor" in the blocks
   which should be a part of the anchor menu.
3. Save layout.

NOTE: This core patch is required for this module to work:
https://www.drupal.org/project/drupal/issues/3015152
