<?php

namespace Drupal\lb_anchor_nav\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a placeholder block to be filled later.
 *
 * @see \Drupal\lb_anchor_nav\EventSubscriber\AnchorNavigationSubscriber;
 * @Block(
 *   id = "anchor_navigation_block",
 *   admin_label = @Translation("Anchor navigation"),
 *   category = @Translation("Anchor navigation")
 * )
 */
class AnchorNavigationBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => $this->t('Placeholder for the "Anchor navigation"
        block. This will be generated for all the blocks that have the
        "Display Anchor" property enabled on this page.'),
    ];
    return $build;
  }

}
