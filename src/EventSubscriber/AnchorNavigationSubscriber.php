<?php

namespace Drupal\lb_anchor_nav\EventSubscriber;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\SectionComponent;
use Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Anchor navigation event subscriber.
 */
class AnchorNavigationSubscriber implements EventSubscriberInterface {

  /**
   * The section storage manager.
   *
   * @var \Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface
   */
  protected $sectionStorageManager;

  /**
   * Constructs event subscriber.
   */
  public function __construct(SectionStorageManagerInterface $sectionStorageManager) {
    $this->sectionStorageManager = $sectionStorageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Layout Builder also subscribes to this event to build the initial render
    // array. We use a higher weight so that we execute after it.
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = [
      'onBuildRender',
      50,
    ];
    return $events;
  }

  /**
   * Add an id to components in the render array and build navigation.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event): void {
    $build = $event->getBuild();

    if (empty($build)) {
      return;
    }

    $sectionStorage = $this->sectionStorageManager->findByContext(
      $event->getContexts(),
      $event->getCacheableMetadata()
    );

    if (empty($sectionStorage)) {
      return;
    }

    $sections = $sectionStorage->getSections();
    if (!$sections) {
      return;
    }

    if ($event->getComponent()->getThirdPartySettings('lb_anchor_nav')) {
      $build['#attributes']['id'] = $event->getComponent()->getUuid();
    }

    if ($event->getPlugin()->getPluginId() === 'anchor_navigation_block') {
      $build['content'] = $this->buildNavigationLinks($sections, $event->getComponent(), $event->getContexts());
    }

    $event->setBuild($build);
  }

  /**
   * Assemble a render array with the link to the components.
   */
  protected function buildNavigationLinks(array $sections, SectionComponent $currentComponent, array $contexts): array {
    $build = [
      '#theme' => 'anchor_navigation',
      '#links' => [],
    ];
    foreach ($sections as $section) {
      foreach ($section->getComponents() as $componentUuid => $component) {
        if ($componentUuid === $currentComponent->getUuid()) {
          continue;
        }

        if (!$component->getThirdPartySettings('lb_anchor_nav')) {
          continue;
        }

        $build['#links'][$componentUuid] = Link::fromTextAndUrl(
          $component->getPlugin($contexts)->label(),
          Url::fromUserInput('#' . $componentUuid)
        )
          ->toRenderable();

        $build['#links'][$componentUuid]['#weight'] = $component->getWeight();
      }
    }
    uasort($build['#links'], [SortArray::class, 'sortByWeightProperty']);
    return $build;
  }

}
